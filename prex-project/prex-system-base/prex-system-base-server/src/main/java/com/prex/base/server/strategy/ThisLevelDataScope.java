package com.prex.base.server.strategy;

import com.prex.base.api.dto.RoleDTO;
import com.prex.base.server.service.ISysUserService;
import com.prex.common.auth.util.PrexSecurityUtil;
import com.prex.common.data.enums.DataScopeTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname ThisLevelHandler
 * @Description 本级
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-06-08 15:44
 * @Version 1.0
 */
@Component("2")
public class ThisLevelDataScope implements AbstractDataScopeHandler {

    @Autowired
    private ISysUserService userService;

    @Override
    public List<Integer> getDeptIds(RoleDTO roleDto, DataScopeTypeEnum dataScopeTypeEnum) {
        // 用于存储部门id
        List<Integer> deptIds = new ArrayList<>();
        deptIds.add(userService.findUserInByName(PrexSecurityUtil.getUser().getUsername()).getDeptId());
        return deptIds;
    }
}
