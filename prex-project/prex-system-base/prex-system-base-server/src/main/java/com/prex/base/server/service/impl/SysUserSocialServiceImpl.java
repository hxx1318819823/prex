package com.prex.base.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prex.base.api.entity.SysUserSocial;
import com.prex.base.server.mapper.SysUserSocialMapper;
import com.prex.base.server.service.ISysUserSocialService;
import org.springframework.stereotype.Service;



/**
 * <p>
 * 社交登录 服务实现类
 * </p>
 *
 * @author lihaodong
 * @since 2019-04-27
 */
@Service
public class SysUserSocialServiceImpl extends ServiceImpl<SysUserSocialMapper, SysUserSocial> implements ISysUserSocialService {


}
